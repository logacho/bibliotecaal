from django.db import models

# Create your models here.
class Genero(models.Model):
    codigo_gen=models.AutoField(primary_key=True)
    nombre_gen=models.CharField(max_length=50)
    descripicon_gen=models.CharField(max_length=50)
    fotografia=models.FileField(upload_to='generos', null=True,blank=True)

class Profesion(models.Model):
    codigo_pro=models.AutoField(primary_key=True)
    nombre_pro=models.CharField(max_length=50)
    descripicon_pro=models.CharField(max_length=50)

class Autor(models.Model):
    codigo_aut=models.AutoField(primary_key=True)
    apellido_aut=models.CharField(max_length=50)
    nombre_aut=models.CharField(max_length=50)
    edad=models.IntegerField()
    archivo_pdf=models.FileField(upload_to='archivos', null=True, blank=True)
    profesion=models.ForeignKey(Profesion,null=True,blank=True,on_delete=models.CASCADE)

class Libro(models.Model):
    codigo_lib=models.AutoField(primary_key=True)
    titulo_lib=models.CharField(max_length=50)
    editorial_lib=models.CharField(max_length=50)
    fecha_publicacion_lib=models.DateField()
    fotografia=models.FileField(upload_to='libros', null=True,blank=True)
    genero=models.ForeignKey(Genero,null=True,blank=True,on_delete=models.CASCADE)
    autor=models.ForeignKey(Autor,null=True,blank=True,on_delete=models.CASCADE)
