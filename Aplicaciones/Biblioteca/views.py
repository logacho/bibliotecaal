from django.shortcuts import render, redirect
from .models import *
from django.contrib import messages

# Create your views here.
def listadoGeneros(request):
    generosBdd=Genero.objects.all()
    return render(request,'genero.html',{'generos':generosBdd})

def guardarGenero(request):
    nombre_gen=request.POST["nombre_gen"]
    descripicon_gen=request.POST["descripicon_gen"]
    fotografia=request.FILES.get("fotografia")

    nuevoGenero=Genero.objects.create(nombre_gen=nombre_gen, descripicon_gen=descripicon_gen, fotografia=fotografia)

    messages.success(request, 'Genero Guardado exitosamente')
    return redirect('/')

def eliminarGenero(request,codigo_gen):
    generoEliminar=Genero.objects.get(codigo_gen=codigo_gen)
    generoEliminar.delete()
    messages.success(request, 'Genero Eliminado exitosamente')
    return redirect('/')

def editarGenero(request,codigo_gen):
    generoEditar=Genero.objects.get(codigo_gen=codigo_gen)
    return render(request,
    'editarGenero.html',{'genero':generoEditar})



def procesarActualizacionGenero(request):
    codigo_gen=request.POST["codigo_gen"]
    nombre_gen=request.POST["nombre_gen"]
    descripicon_gen=request.POST["descripicon_gen"]
    fotografia=request.FILES.get("fotografia")
    generoEditar=Genero.objects.get(codigo_gen=codigo_gen)
    generoEditar.nombre_gen=nombre_gen
    generoEditar.descripicon_gen=descripicon_gen
    if fotografia is not None:
        generoEditar.fotografia=fotografia
    generoEditar.save()
    messages.success(request,'Genero ACTUALIZADO Exitosamente')
    return redirect('/')



def listadoProfesiones(request):
    profesionesBdd=Profesion.objects.all()
    return render(request,'profesion.html',{'profesiones':profesionesBdd, 'navbar': 'profesion'})

def guardarProfesion(request):
    nombre_pro=request.POST["nombre_pro"]
    descripicon_pro=request.POST["descripicon_pro"]

    nuevoProfesion=Profesion.objects.create(nombre_pro=nombre_pro, descripicon_pro=descripicon_pro)

    messages.success(request, 'Profesion Guardado exitosamente')
    return redirect('/profesion')

def eliminarProfesion(request,codigo_pro):
    profesionEliminar=Profesion.objects.get(codigo_pro=codigo_pro)
    profesionEliminar.delete()
    messages.success(request, 'Profesion Eliminado exitosamente')
    return redirect('/profesion')

def editarProfesion(request,codigo_pro):
    profesionEditar=Profesion.objects.get(codigo_pro=codigo_pro)
    return render(request,
    'editarProfesion.html',{'profesion':profesionEditar})

def procesarActualizacionProfesion(request):
    codigo_pro=request.POST["codigo_pro"]
    nombre_pro=request.POST["nombre_pro"]
    descripicon_pro=request.POST["descripicon_pro"]

    #Insertando datos mediante el ORM de DJANGO
    profesionEditar=Profesion.objects.get(codigo_pro=codigo_pro)
    profesionEditar.nombre_pro=nombre_pro
    profesionEditar.descripicon_pro=descripicon_pro


    profesionEditar.save()
    messages.success(request,
      'Profesion ACTUALIZADO Exitosamente')
    return redirect('/profesion')


def listadoAutores(request):
    autoresBdd=Autor.objects.all()
    profesionesBdd=Profesion.objects.all()
    return render(request,'autor.html',{'autores':autoresBdd, 'profesiones':profesionesBdd, 'navbar': 'autor'})

def guardarAutor(request):
    id_pro=request.POST["id_pro"]
    profesionSeleccionado=Profesion.objects.get(codigo_pro=id_pro)
    apellido_aut=request.POST["apellido_aut"]
    nombre_aut=request.POST["nombre_aut"]
    edad=request.POST["edad"]
    archivo_pdf=request.FILES.get("archivo_pdf")


    nuevoAutor=Autor.objects.create(apellido_aut=apellido_aut, nombre_aut=nombre_aut, edad=edad, archivo_pdf=archivo_pdf, profesion=profesionSeleccionado)

    messages.success(request, 'Autor Guardado exitosamente')
    return redirect('/autor')

def eliminarAutor(request,codigo_aut):
    autorEliminar=Autor.objects.get(codigo_aut=codigo_aut)
    autorEliminar.delete()
    messages.success(request, 'Autor Eliminado exitosamente')
    return redirect('/autor')

def editarAutor(request,codigo_aut):
    autorEditar=Autor.objects.get(codigo_aut=codigo_aut)
    profesionesBdd=Profesion.objects.all()
    return render(request,
    'editarAutor.html',{'autor':autorEditar, 'profesiones':profesionesBdd})

def procesarActualizacionAutor(request):
    codigo_aut=request.POST["codigo_aut"]
    id_pro=request.POST["id_pro"]
    profesionSeleccionado=Profesion.objects.get(codigo_pro=id_pro)
    apellido_aut=request.POST["apellido_aut"]
    nombre_aut=request.POST["nombre_aut"]
    edad=request.POST["edad"]

    archivo_pdf=request.FILES.get("archivo_pdf")

    #Insertando datos mediante el ORM de DJANGO
    autorEditar=Autor.objects.get(codigo_aut=codigo_aut)
    autorEditar.profesion=profesionSeleccionado
    autorEditar.apellido_aut=apellido_aut
    autorEditar.nombre_aut=nombre_aut
    autorEditar.edad=edad
    if archivo_pdf is not None:
        autorEditar.archivo_pdf=archivo_pdf

    autorEditar.save()
    messages.success(request,
      'Autor ACTUALIZADO Exitosamente')
    return redirect('/autor')

def listadoLibros(request):
    librosBdd=Libro.objects.all()
    generosBdd=Genero.objects.all()
    autoresBdd=Autor.objects.all()
    return render(request,'libro.html',{'libros':librosBdd, 'generos':generosBdd, 'autores':autoresBdd, 'navbar': 'libro'})

def guardarLibro(request):
    id_gen=request.POST["id_gen"]
    generoSeleccionado=Genero.objects.get(codigo_gen=id_gen)
    id_aut=request.POST["id_aut"]
    autorSeleccionado=Autor.objects.get(codigo_aut=id_aut)
    titulo_lib=request.POST["titulo_lib"]
    editorial_lib=request.POST["editorial_lib"]
    fecha_publicacion_lib=request.POST["fecha_publicacion_lib"]
    fotografia=request.FILES.get("fotografia")

    nuevoLibro=Libro.objects.create(titulo_lib=titulo_lib, editorial_lib=editorial_lib, fecha_publicacion_lib=fecha_publicacion_lib,fotografia=fotografia, genero=generoSeleccionado, autor=autorSeleccionado)

    messages.success(request, 'Libro Guardado exitosamente')
    return redirect('/libro')

def eliminarLibro(request,codigo_lib):
    libroEliminar=Libro.objects.get(codigo_lib=codigo_lib)
    libroEliminar.delete()
    messages.success(request, 'Libro Eliminado exitosamente')
    return redirect('/libro')

def editarLibro(request,codigo_lib):
    libroEditar=Libro.objects.get(codigo_lib=codigo_lib)
    generosBdd=Genero.objects.all()
    autoresBdd=Autor.objects.all()
    return render(request,
    'editarLibro.html',{'libro':libroEditar, 'generos':generosBdd, 'autores':autoresBdd})

def procesarActualizacionLibro(request):
    codigo_lib=request.POST["codigo_lib"]
    id_gen=request.POST["id_gen"]
    generoSeleccionado=Genero.objects.get(codigo_gen=id_gen)
    id_aut=request.POST["id_aut"]
    autorSeleccionado=Autor.objects.get(codigo_aut=id_aut)
    titulo_lib=request.POST["titulo_lib"]
    editorial_lib=request.POST["editorial_lib"]
    fecha_publicacion_lib=request.POST["fecha_publicacion_lib"]
    fotografia=request.FILES.get("fotografia")

    #Insertando datos mediante el ORM de DJANGO
    libroEditar=Libro.objects.get(codigo_lib=codigo_lib)
    libroEditar.genero=generoSeleccionado
    libroEditar.autor=autorSeleccionado
    libroEditar.titulo_lib=titulo_lib
    libroEditar.editorial_lib=editorial_lib
    libroEditar.fecha_publicacion_lib=fecha_publicacion_lib
    if fotografia is not None:
        libroEditar.fotografia=fotografia


    libroEditar.save()
    messages.success(request,
      'Libro ACTUALIZADO Exitosamente')
    return redirect('/libro')
