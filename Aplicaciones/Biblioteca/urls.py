from django.urls import path
from . import views

urlpatterns=[
    path('', views.listadoGeneros, name='genero'),
    path('guardarGenero/', views.guardarGenero),
    path('eliminarGenero/<codigo_gen>', views.eliminarGenero),
    path('editarGenero/<codigo_gen>', views.editarGenero),
    path('procesarActualizacionGenero/', views.procesarActualizacionGenero),

    path('profesion/', views.listadoProfesiones, name='profesion'),
    path('guardarProfesion/', views.guardarProfesion),
    path('eliminarProfesion/<codigo_pro>', views.eliminarProfesion),
    path('editarProfesion/<codigo_pro>', views.editarProfesion),
    path('procesarActualizacionProfesion/', views.procesarActualizacionProfesion),

    path('autor/', views.listadoAutores, name='autor'),
    path('guardarAutor/', views.guardarAutor),
    path('eliminarAutor/<codigo_aut>', views.eliminarAutor),
    path('editarAutor/<codigo_aut>', views.editarAutor),
    path('procesarActualizacionAutor/', views.procesarActualizacionAutor),

    path('libro/', views.listadoLibros, name='libro'),
    path('guardarLibro/', views.guardarLibro),
    path('eliminarLibro/<codigo_lib>', views.eliminarLibro),
    path('editarLibro/<codigo_lib>', views.editarLibro),
    path('procesarActualizacionLibro/', views.procesarActualizacionLibro),





]
