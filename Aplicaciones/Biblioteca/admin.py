from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Genero)
admin.site.register(Profesion)
admin.site.register(Autor)
admin.site.register(Libro)
